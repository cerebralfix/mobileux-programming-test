# README #

## Instructions ##

### Mobile Web ###
We want to test your technical skills, so we need you to make a static mobile website (purely front-end development).

### Functional Requirements ###
1. Present a grid of tiles.
2. User must be able to add and remove tiles.
3. More than one screen or tab.

### What we're looking for ###
* A responsive layout that handles a range of mobile devices.
* Fluid / responsive grid
* Transitions and / or animations
* The process you used to complete this test
* Prudent use of Object Orientated design
* Code reusability
* Extensibility and maintainability of the software
* Use of appropriate framework capabilities (eg. Angular / React / JQuery)
* Your creativity in designing the site

### Extra Credit ###
* Can be used in both orientations; landscape and portrait
* Can be installed as an app (Cordova or similar integration)
* Any extra features or polish you want to include in the site (be creative)

### Deliverables ###
* Instructions on how to run and use your site
* Code should be committed to a git repository hosted on [bitbucket.org](https://bitbucket.org)

## Technology We Use ##
* Node.js
* Typescript
* These are not a requirement, but will be looked on more favourably

## How do I get set up? ##
* [Fork](../../fork) this repository to your bitbucket account, then clone it to your local machine
* Create your site in your new repository.

## Next Steps ##
After you have finished your submission, make sure the reviewers have read access to your repository.
Our developers will review your submission and then invite you in for a discussion.